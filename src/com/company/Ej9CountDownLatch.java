package com.company;

import java.util.concurrent.CountDownLatch;

public class Ej9CountDownLatch {
    private CountDownLatch waitB = new CountDownLatch(1);
    private CountDownLatch waitD = new CountDownLatch(2);
    private CountDownLatch waitG = new CountDownLatch(1);
    private CountDownLatch waitC = new CountDownLatch(1);
    private CountDownLatch waitE = new CountDownLatch(1);
    private CountDownLatch waitH = new CountDownLatch(1);

    private static void sleepRandom(long max) throws InterruptedException {
        Thread.sleep((long) (Math.random() * max));
    }

    public void p1() throws InterruptedException {
        System.out.println("A");
        sleepRandom(100);
        waitD.countDown();
        sleepRandom(100);
        waitB.await();
        System.out.println("B");
        sleepRandom(100);
        waitE.countDown();
        sleepRandom(100);
        waitH.countDown();
        sleepRandom(100);
        waitC.await();
        System.out.println("C");
    }
    public void p2() throws InterruptedException {
        waitD.await();
        System.out.println("D");
        sleepRandom(100);
        waitB.countDown();
        sleepRandom(100);
        waitG.countDown();
        sleepRandom(100);
        waitE.await();
        System.out.println("E");
        sleepRandom(100);
        waitC.countDown();
    }
    public void p3() throws InterruptedException {
        System.out.println("F");
        sleepRandom(100);
        waitD.countDown();
        sleepRandom(100);
        waitG.await();
        System.out.println("G");
        sleepRandom(100);
        waitH.await();
        System.out.println("H");
    }

    public void exec() {
        new Thread(() -> {
            try {
                p1();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                p2();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                p3();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void main(String[] args) {
        new Ej9CountDownLatch().exec();
    }
//Crear e iniciar 3 hilos que ejecutan corredor()
//Crear e iniciar 1 hilo que ejecuta juez()
}
