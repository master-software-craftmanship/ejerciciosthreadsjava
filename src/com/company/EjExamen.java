package com.company;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EjExamen {

    private CyclicBarrier cyclicBarrier;
    private int numeroProcesos;

    private static void sleepRandom(long max) throws InterruptedException {
        Thread.sleep((long) (Math.random() * max));
    }

    public void p(int i) throws InterruptedException {
        for(int j=1; j<5; j++) {
            sleepRandom(100);
            System.out.println(i);
            try {
                cyclicBarrier.await();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    public void exec() {
        numeroProcesos = 3;
        cyclicBarrier = new CyclicBarrier(numeroProcesos, () -> System.out.println('X'));
        new Thread(() -> {
            try {
                p(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                p(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                p(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void main(String[] args) {
        new EjExamen().exec();
    }
//Crear e iniciar 3 hilos que ejecutan corredor()
//Crear e iniciar 1 hilo que ejecuta juez()
}
