package com.company;

public class ProdCons {
    static volatile SincCond sincronizacion = new SincCond();
    static volatile double producto;

    public static void productor() {
        producto = Math.random();
        sincronizacion.signal();
    }
    public static void consumidor() {
        sincronizacion.await();
        System.out.println("Producto: "+producto);
    }
    public void exec() {
        new Thread(()-> productor()).start();
        new Thread(()-> consumidor()).start();
    }
    public static void main(String[] args){
        new ProdCons().exec();
    }
}
