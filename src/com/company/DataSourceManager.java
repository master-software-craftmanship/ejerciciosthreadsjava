package com.company;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DataSourceManager {
    public int dataSource1 = 1;
    public int dataSource2 = 2;
    private Semaphore dataSource1Semaphore = new Semaphore(1, true);
    private Semaphore dataSource2Semaphore = new Semaphore(1, true);
    private Lock lockGarita = new ReentrantLock();
    private Condition conditionDataSourceAny = lockGarita.newCondition();

    public DataSourceManager() {}

    public void accessDataSource(int dataSource) throws InterruptedException {
        if (dataSource == 1) {
            dataSource1Semaphore.acquire();
        } else if (dataSource == 2) {
            dataSource2Semaphore.acquire();
        }
    }

    public int accessAnyDataSource() throws InterruptedException {
        int dataSource;
        lockGarita.lock();
        try {
            while(true) {
                if (dataSource1Semaphore.getQueueLength() == 0 && dataSource1Semaphore.tryAcquire()) {
                    dataSource = dataSource1;
                    break;
                } else if (dataSource2Semaphore.getQueueLength() == 0 && dataSource2Semaphore.tryAcquire()) {
                    dataSource = dataSource2;
                    break;
                } else {
                    conditionDataSourceAny.await();
                }
            }
        } finally {
            lockGarita.unlock();
        }
        return dataSource;
    }

    public void freeDataSource(int dataSource) {
        lockGarita.lock();
        try {
            if (dataSource == dataSource1) {
                dataSource1Semaphore.release();
            } else if (dataSource == dataSource2) {
                dataSource2Semaphore.release();
            }
            conditionDataSourceAny.signal();
        } finally {
            lockGarita.unlock();
        }
    }
}
