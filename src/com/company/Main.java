package com.company;

import java.util.Date;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            Date horaInicio = new Date();
            int iteracion = 0;
            try {
                while(true) {
                    System.out.println("[HILO] Iteracion: " + iteracion);
                    Thread.sleep(333);
                }
            } catch (InterruptedException e) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                System.out.println("[HILO] Me estan interrumpiendo. Tiempo ejecutando=" + (new Date().getTime() - horaInicio.getTime()));
                throw new RuntimeException(e);
            }
        });
        thread.setUncaughtExceptionHandler((th, ex) -> System.out.println("Salgo por interrupción del hilo: " + ex));
        thread.start();

        int seconds = 0;
        while (thread.isAlive()) {
            thread.join(1000);
            seconds++;
            if (seconds >= 5 && thread.isAlive()) {
                System.out.println("[MAIN] Cansado de esperar");
                try {
                    thread.interrupt();
                } catch (RuntimeException e) {
                    System.out.println("[MAIN] Salgo por interrupción");
                }
                thread.join();
            } else {
                System.out.println("[MAIN] Esperando");
            }
        }
        System.out.println("[MAIN] Done!");
    }
}
