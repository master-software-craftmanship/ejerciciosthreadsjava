package com.company;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {
    private CountDownLatch latch = new CountDownLatch(4);
    public void corredor() throws InterruptedException {
        System.out.println("Preparado");
        latch.await();
        System.out.println("Corriendo");
    }
    public void juez() throws InterruptedException {
        for (int i=3; i >= 0; i--) {
            System.out.println(i);
            latch.countDown();
            Thread.sleep(500);
        }
    }

    public void exec() {
        for(int i=0; i<3; i++) {
            new Thread(() -> {
                try {
                    corredor();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        new Thread(() -> {
            try {
                juez();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void main(String[] args) {
        new CountDownLatchDemo().exec();
    }
//Crear e iniciar 3 hilos que ejecutan corredor()
//Crear e iniciar 1 hilo que ejecuta juez()
}
