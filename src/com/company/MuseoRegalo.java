package com.company;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MuseoRegalo {

    static volatile int personas;
    static private Lock cerrojo = new ReentrantLock();

    public static void persona() {

        while (true) {

            cerrojo.lock();
            try {
                personas++;
                System.out.println("hola, somos " + personas);
                if (personas == 1) {
                    System.out.println("Tengo regalo");
                } else {
                    System.out.println("No tengo regalo");
                }
            } finally {
                cerrojo.unlock();
            }

            System.out.println("qué bonito!");
            System.out.println("alucinante!");

            cerrojo.lock();
            try {
                personas--;
                System.out.println("adiós a los " + personas);
            } finally {
                cerrojo.unlock();
            }

            System.out.println("paseo");
        }
    }

    public void exec(int numPersonas) {
        for (int i = 0; i<numPersonas; i++) {
            new Thread(() -> persona()).start();
        }
    }

    public static void main(String[] args) {
        personas = 0;
        new MuseoRegalo().exec(3);
    }
}
