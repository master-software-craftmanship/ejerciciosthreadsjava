package com.company;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BufferSinc {

    private static final int BUFFER_SIZE = 10;

    private int[] datos = new int[BUFFER_SIZE];
    private int posicion = 0;

    private Lock cerrojo = new ReentrantLock();
    private Condition conditionInsertar = cerrojo.newCondition();
    private Condition conditionSacar = cerrojo.newCondition();

    public void insertar(int dato) throws InterruptedException {
        cerrojo.lock();
        try {
            while(posicion == BUFFER_SIZE) {
                conditionInsertar.await();
            }
            datos[posicion] = dato;
            posicion++;
            if (posicion == 1) {
                conditionSacar.signal();
            }
        } finally {
            cerrojo.unlock();
        }
    }

    public int sacar() throws InterruptedException {
        cerrojo.lock();
        int dato;
        try {
            while(posicion == 0) {
                conditionSacar.await();
            }
            posicion--;
            dato = datos[posicion];
            if (posicion == BUFFER_SIZE - 1) {
                conditionInsertar.signal();
            }
        } finally {
            cerrojo.unlock();
        }

        return dato;
    }
}
