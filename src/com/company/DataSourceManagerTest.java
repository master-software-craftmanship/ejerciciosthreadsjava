package com.company;

public class DataSourceManagerTest {

    private DataSourceManager dataSourceManager = new DataSourceManager();

    public void usarDataSource1() {
        try {
            while(true) {
                dataSourceManager.accessDataSource(1);
                System.out.println("UNO Usando source 1");
                sleepRandom(500);
                System.out.println("UNO liberando source 1");
                dataSourceManager.freeDataSource(1);
                sleepRandom(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void usarDataSource2() {
        try {
            while(true) {
                dataSourceManager.accessDataSource(2);
                System.out.println("DOS Usando source 2");
                sleepRandom(500);
                System.out.println("DOS liberando source 2");
                dataSourceManager.freeDataSource(2);
                sleepRandom(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void usarDataSourceAny() {
        try {
            while(true) {
                int dataSource = dataSourceManager.accessAnyDataSource();
                System.out.println("ANY Usando source " + dataSource);
                sleepRandom(500);
                System.out.println("ANY liberando source " + dataSource);
                dataSourceManager.freeDataSource(dataSource);
                sleepRandom(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void sleepRandom(long max) throws InterruptedException {
        Thread.sleep((long) (Math.random() * max));
    }


    public void exec(int numPersonas) {
        for (int i = 0; i<numPersonas; i++) {
            new Thread(() -> usarDataSourceAny()).start();
        }
        for (int i = 0; i<numPersonas; i++) {
            new Thread(() -> usarDataSource1()).start();
        }
        for (int i = 0; i<numPersonas; i++) {
            new Thread(() -> usarDataSource2()).start();
        }
    }

    public static void main(String[] args) {
        new DataSourceManagerTest().exec(3);
    }
}
