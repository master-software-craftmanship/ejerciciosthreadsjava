package com.company;

public class SincCond {
    private volatile boolean producido = false;

    public void await() {
        while (!producido);
    }
    public void signal() {
        producido = true;
    }
}
